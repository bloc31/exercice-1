package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/**
 * Cette classe va gerer la manière dont les données seront représentées
 */
public class BadgesModel extends AbstractTableModel {
        private final String[] entetes = { "ID", "Code Série", "Début", "Fin", "Taille (octets)" };
        private BadgesModel badgeModel;


   List badgesList;

    /**
     * retourne le nombre de lignes
     * @return
     */
        @Override
        public int getRowCount() {
            return badgesList.size();
        }

    /**
     * retourne le nombre de colonnes
     * @return
     */
        @Override
        public int getColumnCount() {
            return entetes.length;
        }

    /**
     * retourne les entetes
     * @param columnIndex  the column being queried
     * @return
     */
    @Override
        public String getColumnName(int columnIndex) {
            return entetes[columnIndex];
        }

    /**
     * donne les valeurs aux cases pour chaque colonne
     * @param rowIndex        the row whose value is to be queried
     * @param columnIndex     the column whose value is to be queried
     * @return
     */
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {

                case 0:

                    return badgesList.get(rowIndex).???;

                case 1:

                    return badgesList.get(rowIndex).getSerial();

                case 2:

                    return badgesList.get(rowIndex).getBegin();

                case 3:

                    return badgesList.get(rowIndex).getEnd();

                case 4:

                    return badgesList.get(rowIndex).????;

                default:
                    throw new IllegalArgumentException();
            }
        }

    /**
     * je comprend pas a quoi ca sert j'ai juste recopié et adapté
     * @param columnIndex  the column being queried
     * @return
     */
    @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0: return Integer.class;

                case 1: return String.class;

                case 3: return Date.class;

                case 2: return Date.class;

                case 4: return Long.class;

                default:
                    return Object.class;
            }
        }

    }
